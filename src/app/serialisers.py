

def serialise_user(user):
    return {"user_id": user.id, "username": user.username, "first_name": user.first_name, "last_name": user.last_name, "phone_number": user.phone_number}