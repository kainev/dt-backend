from telegram import Update
from telegram.ext import CallbackContext
from app.models import AppUser


def start(update: Update, context: CallbackContext):
    user_id = update.effective_user.id
    username = update.effective_user.username
    first_name = update.effective_user.first_name
    last_name = update.effective_chat.last_name
    created = create_user(user_id, username, first_name, last_name)
    update.message.reply_text("Ваши данные записаны в тетрадь смерти 😈 \n Введите /me, чтобы узнать, что мы о вас знаем, либо /help и мы расскажем о доступных командах") if created else update.message.reply_text(
        "Ваши данные уже были записаны, если хотите узнать информацию о себе, напишите /me, либо /help, чтобы узнать о доступных командах")


def create_user(user_id, username, first_name, last_name):
    user, created = AppUser.objects.get_or_create(
        id=user_id, username=username, first_name=first_name, last_name=last_name)
    return created


def me(update: Update, context: CallbackContext):
    user = AppUser.objects.get(id=update.effective_user.id)
    if not user.phone_number:
        text = "Напишешь номерок? 😏 (Тебе поможет команда /set_phone ТВОЙ_НОМЕР) \n А потом я расскажу, что знаю о тебе!"
    else:
        text = f"Username: {user.username}\nFirst name: {user.first_name}\nLast name: {user.last_name}\nPhone number: {user.phone_number}"
    send_message(update, text)


def help(update: Update, context: CallbackContext):
    send_message(update, "Что мы знаем о тебе — /me\nДобавить номер телефона — /set_phone ТВОЙ_НОМЕР")


def set_phone(update: Update, context: CallbackContext):
    user_id = update.effective_user.id
    if len(context.args) == 0:
        text = "Нужно ввести номер телефона после /set_phone"
        send_message(update, text)
        return
    if context.args[0].isdigit():
        AppUser.objects.filter(id=user_id).update(phone_number=context.args[0])
        text = "Номер телефона сохранен."
    else:
        text = "Введен некорректный номер."
    send_message(update, text)


def unknown(update: Update, context: CallbackContext):
    send_message(update, "Я не знаю такой команды. Список доступных команд — /help")


def send_message(update: Update, text):
    update.message.reply_text(text)
