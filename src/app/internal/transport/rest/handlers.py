from django.http import JsonResponse
from django.views import View

from app.models import AppUser
from app.serialisers import serialise_user

class UserDetailView(View):
    def get(self, request, id):
        user = AppUser.objects.get(id=id) 
        return JsonResponse(serialise_user(user), json_dumps_params={'ensure_ascii': False})