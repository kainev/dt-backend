from django.urls import path
from app.internal.transport.rest.handlers import UserDetailView


urlpatterns = [
    path('me/<int:id>', UserDetailView.as_view())
]
