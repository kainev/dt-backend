import environ
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

from app.internal.transport.bot.handlers import start, me, help, set_phone, unknown

env = environ.Env(
    TOKEN=(str, '')
)
updater = Updater(token=env('TOKEN'), use_context=True)
dispatcher = updater.dispatcher

start_handler = CommandHandler('start', start)
me_handler = CommandHandler('me', me)
help_handler = CommandHandler('help', help)
set_phone_handler = CommandHandler('set_phone', set_phone)
unknown_handler = MessageHandler(Filters.command, unknown)
dispatcher.add_handler(start_handler)
dispatcher.add_handler(me_handler)
dispatcher.add_handler(help_handler)
dispatcher.add_handler(set_phone_handler)
dispatcher.add_handler(unknown_handler)
updater.start_polling()
updater.idle()
