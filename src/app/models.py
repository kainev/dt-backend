from app.internal.models.admin_user import AdminUser
from django.db import models

class AppUser(models.Model):
    username = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    phone_number = models.CharField(max_length=12)

def __str__(self):
    return f'{self.username}'

class Meta:
    verbose_name = 'AppUser'
    verbose_name_plural = 'App Users'